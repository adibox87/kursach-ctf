<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::auth();

Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/register', 'GroupController@index')->name('register');
Route::resource('posts', 'PostController');
Route::resource('users', 'UserController');
Route::resource('groups', 'GroupController');
Route::resource('faq', 'FaqController');

Route::get('/scoreboard', 'ScoreboardController@index')->name('scoreboard');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('tasks', 'TaskController');
    Route::resource('news', 'NewsController');
    Route::resource('courses', 'CourseController');
    Route::resource('universities', 'UniversityController');

    Route::get('/post/like/{id}', ['as' => 'post.like', 'uses' => 'PostController@like']);
    Route::get('/post/new/{id}', ['as' => 'post.new', 'uses' => 'PostController@new']);
    Route::get('/task/new/{id}', ['as' => 'task.new', 'uses' => 'TaskController@new']);

    Route::get('/user/role/{id}', ['as' => 'user.role', 'uses' => 'UserController@role']);
    Route::get("search","UserController@search");

    Route::post('courses/{course}/participate', 'ParticipationController@store')->name('participation.store');
    Route::delete('courses/{course}/participate', 'ParticipationController@destroy')->name('participation.destroy');


    Route::group(['prefix' => 'tasks', 'as' => 'tasks.'], function () {
        Route::post('/{task}/submit', 'TaskController@submit')->name('submit');
    });

Route::group(['prefix' => 'news', 'as' => 'news.'], function () {
    Route::post('/{news}/submit', 'NewsController@submit')->name('submit');
});
});




