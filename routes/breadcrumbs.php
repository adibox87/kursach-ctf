<?php

use App\News;
use App\Task;
use App\Post;
use App\Course;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator;

// Home
Breadcrumbs::for('home.index', function (BreadcrumbsGenerator $trail) {
    $trail->push('Главная', route('home.index'));
});

// Scoreboard
Breadcrumbs::for('scoreboard', function (BreadcrumbsGenerator $trail) {
    $trail->parent('home.index');
    $trail->push('Scoreboard', route('scoreboard'));
});

//News
Breadcrumbs::for('news.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('home.index');
    $trail->push('Добавление новости', route('news.create'));
});

//Breadcrumbs::for('news.edit', function (BreadcrumbsGenerator $trail, News $new) {
//    $trail->parent('home.index');
//    $trail->push('Правка новости', route('news.edit', $new));
//});

//Tasks
Breadcrumbs::for('tasks.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('home.index');
    $trail->push('Задания', route('tasks.index'));
});

Breadcrumbs::for('tasks.show', function (BreadcrumbsGenerator $trail, Task $task) {
    $trail->parent('tasks.index');
    $trail->push($task->title, route('tasks.show', $task));
});

Breadcrumbs::for('tasks.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('home.index');
    $trail->push('Создание задания', route('tasks.create'));
});

Breadcrumbs::for('tasks.edit', function (BreadcrumbsGenerator $trail, Task $task) {
    $trail->parent('tasks.show', $task);
    $trail->push('Правка задания', route('tasks.edit', $task));
});

//Posts
Breadcrumbs::for('posts.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('home.index');
    $trail->push('Посты', route('posts.index'));
});

Breadcrumbs::for('posts.show', function (BreadcrumbsGenerator $trail, \App\Post $post) {
    $trail->parent('posts.index');
    $trail->push($post->title, route('posts.show', $post));
});

Breadcrumbs::for('posts.create', function (BreadcrumbsGenerator $trail){
   $trail->parent('posts.index');
   $trail->push('Создание нового поста', route('posts.create'));
});

Breadcrumbs::for('posts.edit', function (BreadcrumbsGenerator $trail, Post $post) {
    $trail->parent('posts.show', $post);
    $trail->push('Правка поста', route('posts.edit', $post));
});

Breadcrumbs::for('faq.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('home.index');
    $trail->push('Faqs', route('faq.index'));
});

//Courses
Breadcrumbs::for('courses.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('home.index');
    $trail->push('Курсы', route('courses.index'));
});

Breadcrumbs::for('courses.show', function (BreadcrumbsGenerator $trail, \App\Course $course) {
    $trail->parent('courses.index');
    $trail->push($course->title, route('courses.show', $course));
});

Breadcrumbs::for('courses.create', function (BreadcrumbsGenerator $trail){
    $trail->parent('courses.index');
    $trail->push('Создание нового курса', route('courses.create'));
});

Breadcrumbs::for('courses.edit', function (BreadcrumbsGenerator $trail, Course $course) {
    $trail->parent('courses.show', $course);
    $trail->push('Правка курса', route('courses.edit', $course));
});

