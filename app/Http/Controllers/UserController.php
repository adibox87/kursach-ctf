<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AdminMiddleware;
use App\Http\Middleware\TeacherOrAdminMiddleware;
use App\Role;
use App\Task;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(TeacherOrAdminMiddleware::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $users = User::orderBy('name')->get();
        return view('users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $tasks = Task::all()->sortBy('id');
        $categories = \App\Category::whereHas('tasks')
            ->whereHas('tasks.submissions', function (Builder $builder) use ($user) {
                return $builder->byUser($user);
            })
            ->with([
                'tasks:cost',
                'tasks.submissions:correct',
            ])
            ->get();

        $submissions = $user->submissions()
            ->withoutGlobalScope('correctOnly')
            ->with(['task'])
            ->get();

        return view('users.show', compact('categories', 'user', 'submissions', 'tasks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        if($request->has('search')){
            $users = User::search($request->get('search'))->orderBy('name')->get();
        }else{
            $users = User::get();
        }
        $roles = Role::all();

        return view('users.index', compact('users','roles'));
    }

    public function role(Request $request, $id)
    {
        if (auth()->user()->isAdmin)
        {
            $user = User::find($id);
            $user->role_id = $request->role_id;
            $user->save();

            return $this->index();
        } else throw new AuthorizationException('You are not allowed');


    }
}
