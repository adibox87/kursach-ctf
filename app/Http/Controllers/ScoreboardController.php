<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\Builder;

class ScoreboardController extends Controller
{
    public function index()
    {
        $contestants = User::contestants()->active()
            ->with([
                'submissions',
                'submissions.task',
            ])
            ->get()
            ->sortByDesc('score')
            ->values();

        return view('scoreboard', compact('contestants'));
    }
}
