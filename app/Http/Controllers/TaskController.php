<?php

namespace App\Http\Controllers;

use App\Category;
use App\Enums\TaskType;
use App\Http\Middleware\TeacherOrAdminMiddleware;
use App\Http\Requests\TaskSubmissionRequest;
use App\Http\Requests\Task\TaskStoreRequest;
use App\Http\Requests\TasksUpdateRequest;
use App\Task;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    public function __construct()
    {
        $this->middleware(TeacherOrAdminMiddleware::class, ['except' => ['index', 'show', 'submit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('title')->get();
        $tasks = Task::orderBy('cost')->get();
        return view('tasks.index', compact('tasks', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $categories = Category::all();
            return view('tasks.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskStoreRequest $request)
    {
        if (auth()->user()->isAdmin || auth()->user()->isTeacher) {
            $task = Task::create($request->validated());

            return redirect(route('tasks.show', compact('task')));
        } else throw new AuthorizationException('You are not allowed');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        $user = auth()->user();

        $submission = $task->submissions()
            ->byUser($user)
            ->when($task->type == TaskType::Manual, function (Builder $builder) {
                return $builder->withoutGlobalScope('correctOnly');
            })
            ->latest()->first();

        return view('tasks.show', compact('task', 'submission', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        if (auth()->user()->isAdmin || auth()->user()->isTeacher) {

            $categories = Category::all();
            return view('tasks.edit', compact('task', 'categories'));

        } else throw new AuthorizationException('You are not allowed');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(TasksUpdateRequest $request, Task $task)
    {

        if (auth()->user()->isAdmin || auth()->user()->isTeacher) {
            $user = auth()->user();
            $task->update($request->validated());
            return view('tasks.show', compact('task', 'user'));

        } else throw new AuthorizationException('You are not allowed');


    }

    public function submit(TaskSubmissionRequest $request, Task $task)
    {
        $validated = $request->validated();

        $submission = $validated['submission'];
        $correct = ($task->type == TaskType::Default && $submission == $task->correct_answer);
        $task->submissions()->create([
            'user_id' => auth()->id(),
            'submission' => $submission,
            'correct' => $correct,
        ]);

        $task->syncChanges();

        if(!$submission == $task->correct_answer){
            return redirect()->back()->with('message', 'danger');
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        if (auth()->user()->isAdmin || auth()->user()->isTeacher) {

            $task->delete();
            return redirect(route('tasks.index'));

        } else throw new AuthorizationException('You are not allowed');
    }

    public function new($id)
    {
        if (auth()->user()->isAdmin || auth()->user()->isTeacher) {

            $categories = Category::all();
            return view('tasks.create', compact('id', 'categories'));

        } else throw new AuthorizationException('You are not allowed');
    }
}
