<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Middleware\TeacherOrAdminMiddleware;
use Illuminate\Http\Request;
use App\Http\Requests\CourseStoreRequest;
use Illuminate\Auth\Access\AuthorizationException;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware(TeacherOrAdminMiddleware::class, ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::orderBy('created_at', 'desc')->get();
        return view('courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseStoreRequest $request)
    {
        $course = Course::create($request->validated());
        return redirect()->route('courses.show',compact('course'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        return view('courses.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        if (auth()->user()->id === $course->user_id || auth()->user()->isAdmin)
        {

            return view('courses.edit', compact('course'));

        } else throw new AuthorizationException('You are not allowed');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(CourseStoreRequest $request, Course $course)
    {

        $course->update($request->validated());
        return redirect()->route('courses.index',compact('course'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {

        $course->delete();
        return redirect(route('courses.index'));

    }
}
