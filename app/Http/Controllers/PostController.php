<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Requests\PostStoreRequest;
use App\Post;
use App\Task;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Task $task
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        $post = Post::create($request->validated());
        return view('posts.show', compact('post'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        if (auth()->user()->id === $post->user_id || auth()->user()->isAdmin)
        {

            return view('posts.edit', compact('post'));

        } else throw new AuthorizationException('You are not allowed');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostStoreRequest $request
     * @param Post $post
     * @return \Illuminate\Http\Response
     * @throws AuthorizationException
     */
    public function update(PostStoreRequest $request, Post $post)
    {

        if (auth()->user()->id === $post->user_id || auth()->user()->isAdmin)
        {
            $post->update($request->validated());
            return view('posts.edit', compact('post'));

        } else throw new AuthorizationException('You are not allowed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect(route('posts.index'));
    }

    public function like($id)
    {
        $post = Post::find($id);
        $post->likeToggle();
        return redirect()->back();
    }

    public function new($id)
    {
        return view('posts.create', compact('id'));
    }
}
