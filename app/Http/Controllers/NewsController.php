<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Requests\NewsStoreRequest;
use App\News;

class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware(AdminMiddleware::class, ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsStoreRequest $request)
    {
        $news = News::create($request->validated());
        return redirect(route('home.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new = News::find($id);
        return view('news.edit', compact('new'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewsStoreRequest $request
     * @param News $new
     * @return \Illuminate\Http\Response
     */
    public function update(NewsStoreRequest $request, $id)
    {
        $new = News::find($id);
        $new->update($request->validated());
        return redirect(route('home.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new = News::find($id);
        $new->delete();
        return redirect(route('home.index'));
    }
}
