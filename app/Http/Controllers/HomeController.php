<?php

namespace App\Http\Controllers;

use App\News;
use App\Group;
use Illuminate\Database\Eloquent\Builder;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $categories = \App\Category::whereHas('tasks')
            ->whereHas('tasks.submissions', function (Builder $builder) use ($user) {
                return $builder->byUser($user);
            })
            ->with([
                'tasks:cost',
                'tasks.submissions:correct',
            ])
            ->get();

        $submissions = $user->submissions()
            ->withoutGlobalScope('correctOnly')
            ->with(['task'])
            ->get();

        $news=News::all()->sortByDesc('updated_at');
        return view('home.index', compact('categories', 'user', 'submissions','news'));
    }
}
