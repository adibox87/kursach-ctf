<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use Illuminate\Http\Response;

class ParticipationController extends Controller
{
    public function store(Course $course)
    {
//        abort_unless($course->users->filter(function (User $user) {
//            return $user->is(auth()->user());
//        })->isEmpty(), Response::HTTP_FORBIDDEN);

        $course->participations()->attach(auth()->id());
        $course->save();

        return back();
    }

    public function destroy(Course $course)
    {
        $course->participations()->detach(auth()->id());
        $course->save();

        return back();
    }

}
