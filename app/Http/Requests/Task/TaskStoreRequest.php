<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class TaskStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isAdmin || auth()->user()->isTeacher;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:2000',
            'lead' => 'required|max:500',
            'description' => 'required|max:2000',
            'cost' => 'required|max:5',
            'correct_answer' => 'required|max:50',
            'category_id' => 'required|max:10',
            'course_id' => 'sometimes|max:10',
            'status' => 'required|max:10',
        ];
    }
}
