<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Access\AuthorizationException;
use Closure;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        if (auth()->user()->isAdmin) {
            return $next($request);
        } else {
            throw new AuthorizationException('You are not allowed');
        }
    }
}
