<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'title',
    ];

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function getColorAttribute()
    {
        return '#' . str_pad(dechex(\Bigwhoop\Crc24\Crc24::hash($this->title) & 0xffffff), 6, "0", STR_PAD_LEFT);
    }
}
