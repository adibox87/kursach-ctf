<?php

namespace App\Console\Commands\Admin;

use App\Category;
use Illuminate\Console\Command;

class CreateCategoryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create:category
                                 {title* : Title of Category}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates category';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $titles = $this->argument('title');
        $bar = $this->output->createProgressBar(count($titles));

        $headers = ['ID', 'Title'];
        $fields = ['id', 'title'];
        $categories = [];

        foreach ($titles as $title) {
            $bar->advance();

            $category = Category::create([
                'title' => $title,
            ])->only($fields);

            array_push($categories, $category);
        }

        $bar->finish();
        $this->line('');

        $this->table($headers, $categories);
    }
}
