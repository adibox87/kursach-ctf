<?php

namespace App\Console\Commands\Admin;

use App\User;
use Hash;
use Illuminate\Console\Command;

class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create:user 
                                {email* : Email of Contestant} 
                                {--P|passwords : Output passwords}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates contestant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = $this->argument('email');
        $bar = $this->output->createProgressBar(count($emails));

        $headers = ['ID', 'Name', 'Email'];
        $fields = ['id', 'name', 'email'];
        $users = [];

        if ($this->option('passwords')) {
            array_push($headers, 'Password');
            array_push($fields, 'password');
        }

        foreach ($emails as $email) {
            $bar->advance();

            $name = strtok($email, '@');
            $password = app()->isLocal() ? 'secret' : str_random();

            $user = User::create([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password)
            ])->only($fields);

            if ($this->option('passwords')) {
                $user['password'] = $password;
            }

            array_push($users, $user);
        }

        $bar->finish();
        $this->line('');

        $this->table($headers, $users);
    }
}
