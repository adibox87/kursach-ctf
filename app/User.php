<?php

namespace App;

use App\Enums\UserRole;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    use SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'role_id',
        'password',
        'group_id'
    ];

    protected $searchable = [
        'columns' => [
            'name' => 15,
            'email' => 10,
        ]
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static function boot()
    {
        parent::boot();
    }

//    public function tasks()
//    {
//        return $this->hasManyThrough(Task::class, Submission::class)->where('submissions.correct', true);
//    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }


    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function submissions()
    {
        return $this->hasMany(Submission::class)->orderBy('submission');
    }

    public function news()
    {
        return $this->hasMany(News::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function participations()
    {
        return $this->belongsToMany(Course::class, 'course_user', 'user_id', 'course_id')
            ->as('participation')
            ->withTimestamps();
    }


    public function getIsAdminAttribute()
    {
        return $this->role_id == UserRole::Admin;
    }

    public function getIsTeacherAttribute()
    {
        return $this->role_id == UserRole::Teacher;
    }

    public function getIsContestantAttribute()
    {
        return $this->role_id == UserRole::Contestant;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getSolvedTasksAttribute()
    {
        return $this->submissions->pluck('task');
    }

    public function getScoreAttribute()
    {
//        return $this->tasks()->sum('cost');
        return $this->solved_tasks->sum('cost');
    }

    public function getScoreForCategory(Category $category)
    {
        return $this->solved_tasks->where('category_id', $category->id)->sum('cost');
    }

    public function scopeContestants(Builder $builder)
    {
        return $builder->where('role_id', UserRole::Contestant);
    }

    public function scopeActive(Builder $builder)
    {
        return $builder->whereHas('submissions', function (Builder $builder) {
            $builder->where('correct', true);
        });
    }
}
