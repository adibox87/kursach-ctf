<?php

namespace App;

use App\Enums\TaskStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title',
        'lead',
        'description',
        'cost',
        'correct_answer',
        'category_id',
        'course_id',
        'status',
        'blocked_by_ids',
    ];

    protected $casts = [
        'cost' => 'integer',
        'blocked_by_ids' => 'array',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('visibleOnly', function (Builder $builder) {
            return $builder->where('status', TaskStatus::Open)
                ->orWhere('status', TaskStatus::Blocked);
        });

        static::addGlobalScope('sortByCost', function (Builder $builder) {
            return $builder->orderBy('cost');
        });
    }

    public function isSolvedBy(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }

        return $this->submissions->where('user_id', $user->id)->count() > 0;
    }

    public function users()
    {
        return $this->hasManyThrough(User::class, Submission::class);
    }

    public function submissions()
    {
        return $this->hasMany(Submission::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class)->withDefault([
            'id' => -1,
            'title' => 'Uncategorized',
        ]);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function getIsSolvedAttribute()
    {
        return $this->isSolvedBy(auth()->user());
    }

    public function getSolvedByAttribute()
    {
        return $this->submissions->pluck('user');
    }

    public function getIsBlockedAttribute()
    {
        return $this->status == TaskStatus::Blocked
            && (!$this->blocked_by->every(function (Task $task) {
                    return $task->isSolvedBy(auth()->user());
            })
                || $this->blocked_by->isEmpty());
    }

    /**
     * @return Task[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function getBlockedByAttribute()
    {
        if (empty($this->blocked_by_ids)) {
            return collect();
        } else {
            return Task::whereIn('id', $this->blocked_by_ids)->get();
        }
    }
}
