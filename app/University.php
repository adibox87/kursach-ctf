<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
