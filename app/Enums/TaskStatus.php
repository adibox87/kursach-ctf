<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class TaskStatus extends Enum
{
    const Hidden = 'hidden';
    const Blocked = 'blocked';
    const Open = 'open';
}
