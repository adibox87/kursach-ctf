<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class TaskType extends Enum
{
    const Default = 'default';
    const Manual = 'manual';
}
