<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class UserRole extends Enum
{
    const Teacher = '3';
    const Contestant = '2';
    const Admin = '1';
}
