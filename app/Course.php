<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'title',
        'lead',
        'user_id',
        'text',
    ];

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function participations()
    {
        return $this->belongsToMany(User::class, 'course_user', 'course_id', 'user_id')
            ->as('participation')
            ->withTimestamps();
    }


    public function getIsParticipatedAttribute()
    {
      return ($this->participations()->where('user_id', auth()->user()->id)->count() > 0 ? true : false);
    }
}
