<?php

namespace App;

use Cog\Likeable\Contracts\Likeable as LikeableContract;
use Cog\Likeable\Traits\Likeable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model implements LikeableContract
{
    use Likeable;

    protected $fillable = [
        'title',
        'lead',
        'user_id',
        'text',
        'task_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
