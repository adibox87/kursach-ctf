<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    protected $fillable = [
        'task_id',
        'user_id',
        'submission',
        'correct'
    ];

    protected $casts = [
        'correct' => 'boolean',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('correctOnly', function (Builder $builder) {
            return $builder->where('correct', true);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function getIsReviewedAttribute()
    {
        return $this->created_at != $this->updated_at;
    }

    public function scopeByUser(Builder $builder, User $user)
    {
        return $builder->where('user_id', $user->id);
    }
}
