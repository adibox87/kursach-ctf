<?php

namespace App\Policies;

use App\Enums\TaskStatus;
use App\Task;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isAdmin || $user->isTeacher) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the task.
     *
     * @param  \App\User $user
     * @param  \App\Task $task
     * @return mixed
     */
    public function view(User $user, Task $task)
    {
        return ($task->status == TaskStatus::Open)
            || ($task->status == TaskStatus::Blocked
                && ($task->blocked_by->every(function (Task $task) use ($user) {
                        return $task->isSolvedBy($user);
                })
                    && $task->blocked_by->isNotEmpty()));
    }

    public function submit(User $user, Task $task)
    {
        return !$task->is_solved && $user->can('view', $task);
    }

    /**
     * Determine whether the user can create tasks.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin || $user->isTeacher;
    }

    /**
     * Determine whether the user can update the task.
     *
     * @param  \App\User $user
     * @param  \App\Task $task
     * @return mixed
     */
    public function update(User $user, Task $task)
    {
        return $user->isAdmin || $user->isTeacher;
    }

    /**
     * Determine whether the user can delete the task.
     *
     * @param  \App\User $user
     * @param  \App\Task $task
     * @return mixed
     */
    public function delete(User $user, Task $task)
    {
        return $user->isAdmin;
    }

    /**
     * Determine whether the user can restore the task.
     *
     * @param  \App\User $user
     * @param  \App\Task $task
     * @return mixed
     */
    public function restore(User $user, Task $task)
    {
        return $user->isAdmin;
    }

    /**
     * Determine whether the user can permanently delete the task.
     *
     * @param  \App\User $user
     * @param  \App\Task $task
     * @return mixed
     */
    public function forceDelete(User $user, Task $task)
    {
        return $user->isAdmin;
    }
}
