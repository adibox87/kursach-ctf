
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('jquery');

require('chart.js');

window.SimpleMDE = require('simplemde');

jQuery(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});

$('.like').on('click', function (event) {
    event.preventDefault();
    $.ajax({
        method: 'GET',
        url: urlLike,
    })
        .done(function(plus_minus){
            var like = document.getElementById("like");
            var likes = document.getElementById("likes");
            var likesAmount = likes.innerText;

            like.classList.toggle("red");

            console.log('success');
            if (like.classList.contains("red") == true) {
                likesAmount ++;
                likes.innerHTML = likesAmount;
            } else {
                likesAmount --;
                likes.innerHTML = likesAmount;
            }
        })
        .fail(function(msg){
            console.log('Failed');
        })
});

