<div id="task-{{ $task->id }}" class="card @if($task->is_solved) border-success @endif">
    <h6 class="card-header">
        {{ $task->title }}<span class="text-muted">&nbsp;&mdash;&nbsp;{{ $task->cost }} {{$task->is_solved}}</span>
    </h6>

    <div class="card-body" >
        @markdown($task->lead)
    </div>

    <div class="card-footer text-muted">
        <div class="btn-group btn-group-justified w-100" role="group" aria-label="Basic example">
            @can('view', $task)
                <a href="{{ route('tasks.show', $task) }}"
                   class="btn btn-sm btn-block @if($task->is_solved) btn-outline-success @else btn-primary @endif"> @if($task->is_solved) Выполнено @else Перейти @endif </a>
            @else
                <button role="button"
                        class="btn btn-sm btn-block btn-outline-primary"
                        data-toggle="popover" data-html="true"
                        title="Task is Blocked"
                        @if($task->blocked_by->isEmpty())
                            data-content="This task is blocked by <b>Administration</b>">
                        @else
                            data-content="This task is blocked by: <ul>{{
                                $task->blocked_by->map(function(\App\Task $task) {
                                    return '<li>' . ($task->is_solved ? '<s>' : '') . '<a href="' . route('tasks.show', $task) . '">' . $task->title . '</a>' . ($task->is_solved ? '</s>' : '') . '</li>';
                                })->implode('')
                            }}</ul>">
                        @endif
                    Заблокированно
                </button>
            @endcan

        </div>
    </div>
</div>
