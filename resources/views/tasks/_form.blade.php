<div class="row">
    <div class="col-md-8 mt-2">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    {{ Form::label('title', 'Заголовок задания') }}
                    {{ Form::text('title', null, ['class' => 'form-control']) }}
                    @if($errors->has('title'))
                        <div class="invalid-feedback">{{$errors->first('title')}}</div>
                    @endif
                </div>

                <div class="form-group">
                    {{ Form::label('lead', 'Аннотация задания') }}
                    {{ Form::textarea('lead', null, ['class' => 'form-control', 'rows' => 5]) }}
                    @if(@isset($id))
                        {{ Form::hidden('course_id', $id, ['class' => 'form-control']) }}
                    @else
                        {{ Form::hidden('course_id', null, ['class' => 'form-control']) }}
                    @endif
                </div>
                <div class="form-group">
                    {{ Form::label('description', 'Описание задания') }}
                    {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => 10]) }}
                    @if($errors->has('description'))
                        <div class="invalid-feedback">{{$errors->first('description')}}</div>
                    @endif
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-4 mt-2">
        <div class="card">
            <div class="card-header">Info</div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ Form::label('correct_answer', 'Answer') }}
                            {{ Form::text('correct_answer', null, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('cost', 'cost') }}
                            {{ Form::number('cost', null, ['class' => 'form-control']) }}
                            @if($errors->has(''))
                                <div class="invalid-feedback">{{$errors->first('cost')}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            {{ Form::label('category_id', 'category_id') }}
                            {{ Form::select('category_id', $categories->pluck('title', 'id')->all() , null, ['class' => 'form-control'], ['placeholder' => 'Pick a category...'])}}

                        </div>
                        <div class="form-group">
                            {{ Form::label('status', 'status') }}
                            {{ Form::select('status', ['open' => 'open', 'blocked' => 'blocked'], null, ['class' => 'form-control'], ['placeholder' => 'Pick a status...'])}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                {{  Form::submit('Сохранить', ['class' => 'btn btn-primary btn-block']) }}
            </div>
        </div>
    </div>
</div>
