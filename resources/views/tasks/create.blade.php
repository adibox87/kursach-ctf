@extends('layouts._app')

@section('content')
    <div class="container">
        {{ Form::open(['role' => 'form', 'id' => 'tasks-form', 'route' => 'tasks.store', 'method' => 'post']) }}
        @include('tasks._form')
        {{ Form::close() }}
    </div>
@endsection