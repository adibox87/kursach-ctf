@extends('layouts._app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col col-12 ">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h5 class="mr-auto">{{$task->title}}<span class="text-muted">&nbsp;&mdash;&nbsp;{{ $task->cost }}</span></h5>
                            <div class="row justify-content-center">
                                <div class="col-4">
                                    @can('update', $task)
                                        <a href="{{route('tasks.edit',$task)}}" class="btn btn-outline-primary">Edit</a>
                                    @endcan
                                </div>
                                <div class="col-5">
                                    @can('delete', $task)
                                        <form method="POST" action="{{route('tasks.destroy', $task)}}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-outline-danger" type="submit">Delete</button>
                                        </form>
                                    @endcan
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if($task->course_id)
                            @include('tasks.alerts.course')
                        @endif
                        @if($task->is_solved)
                            @include('tasks.alerts.solved')
                        @endif

                        @if($task->is_blocked)
                            @include('tasks.alerts.blocked')
                        @endif
                        @markdown($task->description)
                        {{$task->correct_answer}}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col col-12 ">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h5 class="mr-auto ">Разборы задания <span class="text-muted">&nbsp;&mdash;&nbsp;{{ $task->posts()->count() }}</span></h5>
                            <div>
                                <a class="btn btn-outline-primary" href="{{route('post.new', $task->id)}}">Создать свой пост</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="card-columns">

                            @foreach($task->posts as $post)
                                @include('posts._card')
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @can('edit', $task)
        <div class="row">
            <div class="col col-12 ">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h5 class="mr-auto ">Правильных ответов <span class="text-muted">&nbsp;&mdash;&nbsp;{{ $task->solved_by->count() }}</span></h5>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover text-decoration-none">
                            <tr>
                                <th>Имя пользователя</th>
                                <th>Попытки решения</th>
                                <th>Количество очков</th>
                            </tr>
                            @foreach($task->solved_by as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$task->submissions()->byUser($user)->withoutGlobalScope('correctOnly')->count()}}</td>
                                    <td>{{$user->score}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endcan

        @includeIf('tasks.submission.default')
    </div>
@endsection
