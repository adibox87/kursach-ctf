@can('submit', $task)
    <div class="row">
        <div class="col col-12 ">
            <div class="card">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" id="submit-tab" data-toggle="tab" href="#submit"
                               role="tab"
                               aria-controls="home" aria-selected="true">Submit</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="previous-tab" data-toggle="tab" href="#previous" role="tab"
                               aria-controls="home" aria-selected="false">Previous</a>
                        </li>
                    </ul>
                </div>

                <div class="card-body">
                    <div class="tab-content">

                        <div class="tab-pane active" id="submit" role="tabpanel" aria-labelledby="submit-tab">
                            <form method="POST" action="{{ route('tasks.submit', $task) }}">
                                @csrf

                                <div class="form-group row">
                                    <div class="col col-12">
                                    @if(session()->has('message'))
                                        <div class="alert alert-{{ session()->get('message') }}">

                                            Your answer is incorrect

                                        </div>
                                    @endif
                                        <label for="submission">Your answer:</label>
                                        <textarea rows="3" class="form-control"
                                                  id="submission" name="submission"
                                                  @if($task->is_solved) disabled @endif>{{ old('submission') }}</textarea>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary "
                                        @if($task->is_solved) disabled @endif>Submit
                                </button>
                            </form>
                        </div>

                        <div class="tab-pane" id="previous" role="tabpanel" aria-labelledby="previous-tab">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Submission</th>
                                    <th scope="col">Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($task->submissions()->byUser($user)->withoutGlobalScope('correctOnly')->get() as $submission)
                                    <tr class="@if($submission->correct) table-success @endif">
                                        <td><code>{{ $submission->submission }}</code></td>
                                        <td>{{ $submission->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endcan
