<div class="row">
    <div class="col col-12 ">
        <div class="card">
            <div class="card-header">
                Submit
            </div>

            <div class="card-body">
                @if(empty($submission) || (!$submission->correct && $submission->is_reviewed))
                    @if(optional($submission)->is_reviewed)
                        <div class="alert alert-danger" role="alert">
                            Your submission has been rejected!
                        </div>
                    @endif

                    <form method="POST" action="{{ route('tasks.submit', $task) }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col col-12">
                                <textarea id="submission" class="form-control" name="submission"></textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary "
                                @if($task->is_solved) disabled @endif>Submit
                        </button>
                    </form>
                @else
                    @if($submission->correct)
                        <div class="alert alert-success" role="alert">
                            Your submission has been reviewed and accepted!
                        </div>
                    @elseif($submission->is_reviewed)
                        <div class="alert alert-danger" role="alert">
                            Your submission has been rejected!
                        </div>
                    @else
                        <div class="alert alert-info" role="alert">
                            Your submission is being reviewed
                        </div>
                    @endif

                    @markdown($submission->submission)
                @endif
            </div>
        </div>
    </div>
</div>

@push('html-body-bottom')
    <script>
        window.addEventListener('DOMContentLoaded', function () {
            var simplemde = new SimpleMDE({
                element: document.getElementById("submission"),
                autofocus: true,
                autosave: {
                    enabled: true,
                    uniqueId: "task-{{ $task->id }}",
                    delay: 1000,
                },
                forceSync: true,
                placeholder: "Type here...",
                showIcons: ["code", "table"],
                spellChecker: false,
            });
        })
    </script>
@endpush
