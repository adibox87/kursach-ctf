@extends('layouts._app')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($categories as $category)
                <div class="col col-12 mb-4">
                    <h3>{{ $category->title }}</h3>

                    <div class="card-columns">
                        @foreach($category->tasks as $task)
                            @include('tasks._card')
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
