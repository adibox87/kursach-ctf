@extends('layouts._app')

@section('content')
    <div class="container">
        {{ Form::model($task,['role' => 'form', 'id' => 'tasks-form', 'route' => ['tasks.update',$task->id], 'method' => 'put']) }}
        @include('tasks._form')
        {{ Form::close() }}
    </div>
@endsection