<div class="alert alert-info" role="alert">
    <h4 class="alert-heading">Задание к курсу {{$task->course->title}}</h4>
    <p class="mb-0">
        <a href="{{ route('courses.show', $task->course) }}">Перейти к курсу</a>
    </p>
</div>
