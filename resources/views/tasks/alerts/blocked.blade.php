<div class="alert alert-warning" role="alert">
    <h4 class="alert-heading">The task is blocked!</h4>
    @if($task->blocked_by->isEmpty())
        <p class="mb-0">The task is blocked by <strong>Administration</strong></p>
    @else
        <p>You have to solve this tasks at first:</p>
        <ul>
            @foreach($task->blocked_by as $blocked_by)
                <li>
                    @if($blocked_by->is_solved)
                        <s>
                            @endif
                            <a href="{{ route('tasks.show', $blocked_by) }}">{{ $blocked_by->title }}</a>
                            @if($blocked_by->is_solved)
                        </s>
                    @endif
                </li>
            @endforeach
        </ul>
    @endif
</div>
