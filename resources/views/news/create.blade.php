@extends('layouts._app')

@section('content')
    <div class="container">
        {{ Form::open(['role' => 'form', 'id' => 'news-form', 'route' => 'news.store', 'method' => 'post']) }}
        @include('news._form')
        {{ Form::close() }}
    </div>
@endsection