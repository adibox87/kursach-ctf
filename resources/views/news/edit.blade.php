@extends('layouts._app')

@section('content')
    <div class="container">
        {{ Form::model($new,['role' => 'form', 'id' => 'news-form', 'route' => ['news.update',$new], 'method' => 'put']) }}
        @include('news._form')
        {{ Form::close() }}
    </div>
@endsection
