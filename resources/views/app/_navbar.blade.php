<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'CTF') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('tasks.index') }}">{{ __('Задания') }}</a>
                    </li>


                    @if(auth()->user()->isTeacher || auth()->user()->isAdmin)
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('courses.create')}}">Cоздать курс</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('users.index')}}">Пользователи</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('universities.index')}}">Университеты</a>
                        </li>
                    @endif
                @endauth



                <li class="nav-item">
                    <a class="nav-link" href="{{ route('scoreboard') }}">{{ __('Scoreboard') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('posts.index')}}">Разборы</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('courses.index')}}">Курсы</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('faq.index')}}">FAQ</a>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    <li class="nav-item">
                        @if (Route::has('register'))
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}

                            @if(Auth::user()->group)
                                <span class="text-info">{{Auth::user()->group->shipher}}</span>
                            @endif
                            ({{ Auth::user()->score }})<span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Выйти') }}
                            </a>

                    @if(auth()->user()->isAdmin)

                            <a class="dropdown-item" href="{{route('tasks.create')}}">Создать задание</a>



                            <a class="dropdown-item" href="{{route('news.create')}}">Создать новость</a>

                    @endif

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
