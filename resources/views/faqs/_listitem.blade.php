<div class="col-md-12 mx-auto">


    <div class="border-bottom py-2">
        <a class="h2" style="text-decoration: none" data-toggle="collapse" href="#collapseExample{{ $faq->id }}" aria-expanded="false" aria-controls="collapseExample">
           {{ $faq -> question }}
            <i class="float-right mr-3 mt-1 fas fa-plus fa-sm"></i>
        </a>
    </div>
    <div class="collapse" id="collapseExample{{ $faq->id }}">
        <div class="row">
            <div class="col-md-12 py-2">
                <p class="h5">
                    {{ $faq -> answer }}
                </p>
            </div>
        </div>
    </div>
</div>
