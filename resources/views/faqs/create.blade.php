@extends('layouts._app')

@section('content')
    <div class="container">
        {{ Form::open(['role' => 'form', 'id' => 'faqs-form', 'route' => 'faq.store', 'method' => 'post']) }}
        @include('faqs._form')
        {{ Form::close() }}
    </div>
@endsection
