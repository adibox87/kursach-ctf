@extends('layouts._app')

@section('content')
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12">
                @auth
                    @if(auth()->user()->isAdmin)
                        <a class="btn btn-outline-primary" href="{{route('faq.create')}}">Создать faq</a>
                    @endif
                @endauth

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1>Ответы на часто задаваемые вопросы</h1>
            </div>
        </div>
        <div class="row">
                    @foreach($faqs as $faq)
                        @include('faqs._listitem')
                    @endforeach
        </div>
    </div>
@endsection
