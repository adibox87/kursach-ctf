<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <title>{{ @$title }}</title>

    @stack('html-head-bottom')

</head>
<body class="{{ @$bodyClass }} {{ (preg_match("/Win/", @$_SERVER['HTTP_USER_AGENT']) || substr(@$_SERVER['HTTP_HOST'], -5) == '.home') ? 'segoe' : '' }}">

@stack('html-body-top')

@yield('html-body')

@stack('html-body-bottom')

</body>
</html>
