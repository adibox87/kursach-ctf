@extends('layouts._html', [
    'title' => @$title ?: @$appbarTitle,
])

@push('html-head-bottom')
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor.css') }}" rel="stylesheet">
@endpush

@push('html-body-top')

@endpush

@push('html-body-bottom')

@endpush

@section('navbar')
    @include('app._navbar')
@endsection

@section('app-content')
    @yield('content')
@endsection

@section('html-body')
    <div id="app">
        @yield('navbar')

        <main class="py-4">
            @yield('app-content')
        </main>
    </div>
@endsection
