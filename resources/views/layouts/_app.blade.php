@extends('layouts._simple', [
    'bodyClass' => @$bodyClass . ' has-sidenav-fixed',
    'title' => ($breadcrumb = Breadcrumbs::current())
                ? $breadcrumb->title. ' :: ' . config('app.name', 'Laravel')
                : config('app.name', 'Laravel')
])

@section('breadcrumbs', Breadcrumbs::render())

@section('app-content')

    <div class="container">
        @yield('breadcrumbs')
    </div>

    @yield('content')

@endsection
