@extends('layouts._app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col col-12 col-md-4">
                @include('home.cards.category_breakdown')

                @include('home.cards.submission_percentage')
            </div>

            <div class="col col-12 col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3>{{ $user->name }} <span class="mr-2 float-right text-info" > Счет: {{$user->score}}</span></h3>

                    </div>

                    <div class="card-body">
                        <h4 class="card-title">Заданий выполнено {{$user->solved_tasks->count()}} из {{$tasks->count()}}</h4>
                        <ul class="list-unstyled">
                            <li>Права: {{$user->role->name}}</li>
                            @if($user->group)<li>Группа: {{$user->group->shipher}}</li>@endif
                            @if($user->group)<li>Институт: {{$user->group->university->name}}</li>@endif
                        </ul>
                    </div>

                </div>
            </div>

        </div>
        @if($user->solved_tasks->count() > 0)
            <div class="row mt-5">
                <div class="col col-12">
                    <h3>Решенные задания ({{ $user->solved_tasks->count() }})</h3>
                </div>
                <div class="col-12">
                        <table class="table table-hover text-decoration-none">
                            <tr>
                                <th>Название задания</th>
                                <th>Количество очков</th>
                                <th>Попытки решения</th>
                                <th>Верный ответ</th>
                            </tr>
                            @foreach($user->solved_tasks as $task)
                            <tr>
                                <td>{{$task->title}}</td>
                                <td>{{$task->cost}}</td>
                                <td>{{$task->submissions()->byUser($user)->withoutGlobalScope('correctOnly')->count()}}</td>
                                <td>{{$task->correct_answer}}</td>
                            </tr>
                            @endforeach
                        </table>

                </div>
            </div>
        @endif

        @if($user->posts->count() > 0)
            <div class="row mt-5">
                <div class="col col-12">
                    <h3>Разборы пользователя ({{ $user->posts->count() }})</h3>
                </div>
                <div class="col col-12">
                    <div class="card-columns">
                        @foreach($user->posts as $post)
                            @include('posts._card')
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        @if($user->participations->count() > 0)
            <div class="row mt-3">
                <div class="col col-12">
                    <h3>Курсы ({{ $user->participations->count() }})</h3>
                </div>
                <div class="col col-12">

                    @foreach($user->participations as $course)
                        <a href="{{ route('courses.show', $course) }}" class="list-group-item list-group-item-action">
                            @include('courses._card')
                        </a>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
@endsection

