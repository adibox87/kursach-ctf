@extends('layouts._app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="GET" action="{{url('search')}}">
                    <div class="row">
                        <div class="col-md-10">
                            <input type="text" name="search" class="form-control" placeholder="Search" value="{{ old('search') }}">
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-block btn-success">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row pt-3">
            <div class="col-md-12">
                <table class="table table-hover text-decoration-none">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                    </tr>
                    @if($users->count())
                        @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td><a href="{{ route('users.show', $user) }}">{{ $user->name }}</a></td>
                                    <td>{{ $user->email }}</td>

                                    @if(auth()->user()->isAdmin)
                                        <td>
                                            <form method="GET" action="{{route('user.role', $user->id)}}">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <select id='role_id' class="form-control" name='role_id'>
                                                            @foreach($roles as $role)
                                                                <option @if($user->role_id === $role->id) selected @endif value='{{$role->id}}'>{{$role->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-4">
                                                        <button class="btn btn-block btn-success">Change</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                    @else
                                        <td>{{ $user->role->name }}</td>
                                    @endif
                                </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">Result not found.</td>
                        </tr>
                    @endif
                </table>
            </div>

        </div>
    </div>

@endsection
