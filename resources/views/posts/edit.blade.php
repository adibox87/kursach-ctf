@extends('layouts._app')

@section('content')
    <div class="container">
        {{ Form::model($post,['role' => 'form', 'id' => 'posts-form', 'route' => ['posts.update',$post->id], 'method' => 'put']) }}
        @include('posts._form')
        {{ Form::close() }}
    </div>
@endsection
