@extends('layouts._app')

@section('content')
    <div class="container">
        {{ Form::open(['role' => 'form', 'id' => 'posts-form', 'route' => 'posts.store', 'method' => 'post']) }}
        @include('posts._form')
        {{ Form::close() }}
    </div>
@endsection
