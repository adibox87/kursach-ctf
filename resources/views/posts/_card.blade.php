<div id="task-{{ $post->id }}" class="card">
    <div class="card-header">
        <h5 class="text-truncate">{{ $post->title }} <span class="float-right mr-2"><i class="fas fa-heart fa-md text-danger"></i><span class="ml-2" id="likes">{{$post->likesCount}}</span></span></h5>
        <h6 class="text-truncate">{{ $post->task->title }} </h6>
    </div>
    <div class="card-body">
        @markdown($post->lead)
        <a href="{{ route('posts.show', $post) }}"
           class="btn btn-lg btn-block btn-outline-primary"> Подробнее </a>
    </div>
</div>
