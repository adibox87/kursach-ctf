@extends('layouts._app')

@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col-md-12">
                <h1>Разборы</h1>
            </div>
        </div>
        <div class="row">
            <div class="col col-12 mb-4">
                <div class="card-columns">
                    @foreach($posts as $post)
                        @include('posts._card')
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
