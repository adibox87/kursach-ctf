@extends('layouts._app')

@section('content')
    <div class="container">
            <div class="row">
            <div class="col col-12 ">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8">
                                <h5>{{ $post->title }}</h5>
                                Автор : {{$post->user->name}}
                                @auth
                                    <a class="like @if($post->liked) red @endif" id="like"><div class="new-length"><i class="fas fa-heart fa-lg"></i><span class="ml-2" id="likes">{{$post->likesCount}}</span></div></a>

                                @endauth
                                @guest
                                    <div><i class="fas fa-heart fa-lg"></i><span class="ml-2" id="likes">{{$post->likesCount}}</span></div>
                                @endguest
                            </div>
                            <div class="col-md-2">
                                @can('update', $post)
                                    <a href="{{route('posts.edit',$post->id)}}" class="btn btn-primary btn-block">Edit</a>
                                @endcan


                            </div>
                            <div class="col-md-2">
                                @can('delete', $post)
                                    <form method="POST" action="{{route('posts.destroy', $post)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-block" type="submit">Delete</button>
                                    </form>
                                @endcan
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @markdown($post->text)
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        const urlLike = '{{ route('post.like', $post) }}';
    </script>
@endsection
