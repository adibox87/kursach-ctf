@extends('layouts._app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">Scoreboard</div>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Score</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contestants as $place => $contestant)
                            <tr>
                                <td>{{ $place + 1 }}</td>
                                <td>{{ $contestant->name }}</td>
                                <td>{{ $contestant->score }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
