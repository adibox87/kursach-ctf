<div class="card mb-3">
    <div class="card-header">
        Submission percentage
    </div>
    <div class="card-body">
        <canvas id="submissionPercentage"></canvas>
    </div>
</div>

@push('html-body-bottom')
    <script>
        window.addEventListener('DOMContentLoaded', function () {
            let solves = {{$user->submissions->count()}}
            let fails = {{$user->submissions()->withoutGlobalScope('correctOnly')->count()}} - {{$user->submissions->count()}}

            var submissionPercentageData = {
                labels: {!! collect(['Fails', 'Solves'])->toJson() !!},
                datasets: [
                    {
                        backgroundColor: ['#cf2600', '#00d140'],
                        borderWidth: 0,
                        data: [fails, solves]
                    }
                ]
            };

            var submissionPercentageOptions = {
                cutoutPercentage: 85,
                legend: {position: 'right', padding: 5, labels: {pointStyle: 'circle', usePointStyle: true}}
            };

            new Chart($('#submissionPercentage'), {
                type: 'pie',
                data: submissionPercentageData,
                options: submissionPercentageOptions
            });
        })
    </script>
@endpush
