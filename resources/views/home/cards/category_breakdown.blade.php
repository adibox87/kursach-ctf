<div class="card mb-3">
    <div class="card-header">
        Category breakdown
    </div>
    <div class="card-body">
        <canvas id="categoryBreakdown"></canvas>
    </div>
</div>

@push('html-body-bottom')
    <script>
        window.addEventListener('DOMContentLoaded', function () {

            var categoryBreakdownData = {
                labels: {!! $categories->pluck('title')->toJson() !!},
                datasets: [
                    {
                        backgroundColor: {!!
                            $categories->map(function (\App\Category $category) use ($user) {
                                return $category->color;
                            })->toJson()
                        !!},
                        borderWidth: 0,
                        data: {!!
                            $categories->map(function (\App\Category $category) use ($user) {
                                return $user->getScoreForCategory($category);
                            })->toJson()
                        !!}
                    }
                ]
            };

            var categoryBreakdownOptions = {
                cutoutPercentage: 85,
                legend: {position: 'right', padding: 5, labels: {pointStyle: 'circle', usePointStyle: true}}
            };

            new Chart($('#categoryBreakdown'), {
                type: 'pie',
                data: categoryBreakdownData,
                options: categoryBreakdownOptions
            });
        })
    </script>
@endpush
