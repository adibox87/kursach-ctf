@extends('layouts._app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col col-12 col-md-4">
                @include('home.cards.category_breakdown')

                @include('home.cards.submission_percentage')
            </div>

            <div class="col col-12 col-md-8">
                <div class="card mb-3">
                    <div class="card-header">News</div>
                    @foreach($news as $new)
                    <div class="card-body">
                         <h4>{{$new->title}}</h4>
                        <div class="row">
                            <div class="col-md-3">
                                {{$new->updated_at->diffForHumans()}}
                            </div>
                             <div class="col-md-6">{{$new->user->name}}</div>
                        </div>
                            <p>{{$new->body}}</p>
                        <div class="row">
                            <div class="col-md-6">
                                @can('update', $new)
                                    <a href="{{route('news.edit',$new)}}" class="btn btn-block btn-primary">Edit</a>
                                @endcan
                            </div>

                            <div class="col-md-6">
                                @can('delete', $new)
                                    <form method="POST" action="{{route('news.destroy', $new)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-block btn-danger" type="submit">Delete</button>
                                    </form>
                                @endcan
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @if($user->solved_tasks->count() > 0)
            <div class="row mt-3">
                <div class="col col-12">
                    <h3>Solves ({{ $user->solved_tasks->count() }})</h3>
                </div>
                <div class="col col-12">
                    <div class="card-columns">
                        @foreach($user->solved_tasks as $task)
                            @include('tasks._card')
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        @if($user->courses->count() > 0)
            <div class="row mt-3">
                <div class="col col-12">
                    <h3>Мои Курсы ({{ $user->courses->count() }})</h3>
                </div>
                <div class="col col-12">

                    @foreach($user->courses as $course)
                        <a href="{{ route('courses.show', $course) }}" class="list-group-item list-group-item-action">
                            @include('courses._card')
                        </a>
                    @endforeach
                </div>
            </div>
        @endif

        @if($user->participations->count() > 0)
            <div class="row mt-3">
                <div class="col col-12">
                    <h3>Курсы на которые я записан ({{ $user->participations->count() }})</h3>
                </div>
                <div class="col col-12">

                        @foreach($user->participations as $course)
                        <a href="{{ route('courses.show', $course) }}" class="list-group-item list-group-item-action">
                            @include('courses._card')
                        </a>
                        @endforeach
                </div>
            </div>
        @endif
    </div>
@endsection
