@extends('layouts._app')

@section('content')
<div class="container">
    <div class="row pt-3">
        <div class="col-md-12">
                <h3 class="pb-3">{{$group->shipher}}</h3>
                @if($group->users->count())
                <table class="table table-hover text-decoration-none">
                    <tr>
                        <th>Имя пользоваетеля</th>
                        <th class="text-right">Количество очков</th>

                    </tr>
                    @foreach($group->users as $user)
                        <tr>
                            <td><a href="{{ route('users.show', $user) }}">{{$user->name}}</a></td>
                            <td class="text-right">{{ $user->score }}</td>
                        </tr>
                    @endforeach
                </table>
                @else
                    <h3>Студенты отсутвуют</h3>
                @endif
        </div>
    </div>
</div>
@endsection
