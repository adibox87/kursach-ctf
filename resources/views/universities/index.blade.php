@extends('layouts._app')

@section('content')

    <div class="container">
        <div class="row pt-3">
            <div class="col-md-12">
                @foreach($universities as $university)
                    <h3>{{$university->name}}</h3>
                    <table class="table table-hover text-decoration-none">
                        <tr>
                            <th>Шифр</th>
                            <th class="text-right">Количество студентов</th>

                        </tr>
                            @foreach($university->groups as $group)
                                <tr>
                                    <td><a href="{{ route('universities.show', $group->id) }}">{{$group->shipher}}</a></td>
                                    <td class="text-right">{{ $group->users->count() }}</td>

                                </tr>
                            @endforeach
                    </table>

                    @endforeach
            </div>
        </div>
    </div>


@endsection

