@extends('layouts._app')

@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col-md-12">
                <h1>Курсы</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-7 mb-4">
                    @foreach($courses as $course)
                        <div class="list-group">
                            <a href="{{ route('courses.show', $course) }}" class="list-group-item list-group-item-action @if($course->is_participated) active @endif">
                            @include('courses._card')
                            </a>
                        </div>

                    @endforeach
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Всего курсов: {{$courses->count()}}</h5>
                    </div>
                    <div class="card-body">
                        <p>В этом разделе системы вы можете ознакомиться с курсами, а так-же записаться на них</p>
                    </div>
                    <div class="card-footer">
                        <small>Синим подсвечены курсы на которые вы уже записаны</small>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
