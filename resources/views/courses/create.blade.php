@extends('layouts._app')

@section('content')
    <div class="container">
        {{ Form::open(['role' => 'form', 'id' => 'courses-form', 'route' => 'courses.store', 'method' => 'post']) }}
        @include('courses._form')
        {{ Form::close() }}
    </div>
@endsection
