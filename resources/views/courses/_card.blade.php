
    <div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1">{{$course->title}}</h5>

        <small>{{$course->participations->count()}}</small>
    </div>
    <p class="mb-1">{{$course->lead}}</p>
    <small>Преподаватель: {{$course->user->name}}</small>
