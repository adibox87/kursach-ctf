@extends('layouts._app')

@section('content')
    <div class="container">
        {{ Form::model($course,['role' => 'form', 'id' => 'courses-form', 'route' => ['courses.update',$course->id], 'method' => 'put']) }}
        @include('courses._form')
        {{ Form::close() }}
    </div>
@endsection
