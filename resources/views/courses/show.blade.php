@extends('layouts._app')

@section('content')
    <div class="container">
            <div class="row">
            <div class="col col-12 ">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h4 class="mr-auto">{{$course->title}}</h4>
                            <div class="row justify-content-end">
                                <div class="col-4">
                                    @can('update', $course)
                                        <a href="{{route('courses.edit',$course)}}" class="btn btn-outline-primary">Edit</a>
                                    @endcan
                                </div>
                                <div class="col-6">
                                    @can('delete', $course)
                                        <form method="POST" action="{{route('courses.destroy', $course)}}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-outline-danger" type="submit">Delete</button>
                                        </form>
                                    @endcan
                                </div>

                            </div>
                        </div>
                        <div class="d-flex"><h5 class="mr-auto">Преподаватель:{{$course->user->name}}</h5></div>
                    </div>

                    <div class="card-body">

                        @markdown($course->text)

                        @if($course->participations->filter(function (\App\User $user) { return $user->is(auth()->user()); })->isEmpty())
                            <form method="post" action="{{ route('participation.store', $course) }}">
                                @csrf
                                <button class="btn btn-primary btn-block" type="submit">Участвовать</button>
                            </form>
                        @else
                            <form method="post" action="{{ route('participation.destroy', $course) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-outline-danger btn-block" type="submit">Отменить участие</button>
                            </form>
                        @endif


                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col col-12 ">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h5 class="mr-auto ">Задания к курсу <span class="text-muted">&nbsp;&mdash;&nbsp;{{ $course->tasks()->count() }}</span></h5>
                            @can('update', $course)
                            <div>
                                <a class="btn btn-outline-primary" href="{{route('task.new', $course->id)}}">Создать задание к курсу</a>
                            </div>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        @if($course->tasks()->count())
                        <div class="card-columns">

                            @foreach($course->tasks as $task)
                                @include('tasks._card')
                            @endforeach
                        </div>
                        @else
                        <h5>Заданий пока нет</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-12 ">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h5 class="mr-auto ">Участники курса <span class="text-muted">&nbsp;&mdash;&nbsp;{{ $course->participations()->count() }}</span></h5>
                        </div>
                    </div>
                    <div class="card-body">
                        @if($course->participations()->count())

                                @foreach($course->participations as $user)
                                <a href="{{route('users.show', $user)}}">{{$user->name}}</a>,
                                @endforeach
                        @else
                            <h5>Пока никто не записан</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
