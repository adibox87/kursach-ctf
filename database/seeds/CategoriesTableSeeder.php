<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Uncategorized',
                'sort' => 100,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),

            1 =>
                array (
                    'id' => 2,
                    'title' => 'Web',
                    'sort' => 100,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            2 =>
                array (
                    'id' => 3,
                    'title' => 'Reverse',
                    'sort' => 100,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            3 =>
                array (
                    'id' => 4,
                    'title' => 'PWN',
                    'sort' => 100,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            4 =>
                array (
                    'id' => 5,
                    'title' => 'Crypto',
                    'sort' => 100,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            5 =>
                array (
                    'id' => 6,
                    'title' => 'Stegano',
                    'sort' => 100,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),

            6 =>
                array (
                    'id' => 7,
                    'title' => 'Joy',
                    'sort' => 100,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
        ));


        
        
    }
}