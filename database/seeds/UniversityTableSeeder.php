<?php

use Illuminate\Database\Seeder;

class UniversityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('universities')->delete();

        \DB::table('universities')->insert(array (
            1 =>
                array (
                    'name' => 'Институт комплексной безопасности и специального приборостроения',
                    'address' => 'Москва',
                ),

            2 =>
                array (
                    'name' => 'Институт информационных технологий',
                    'address' => 'Москва',
                ),

            3 =>
            array (
                'name' => 'Институт кибернетики',
                'address' => 'Москва',
            ),

            4 =>
            array (
                'name' => 'Институт радиотехнических и телекоммуникационных систем',
                'address' => 'Москва',
            ),

            5 =>
            array (
                'name' => 'Физико-технологический институт',
                'address' => 'Москва',
            ),

            6 =>
            array (
                'name' => 'Институт инновационных технологий и государственного управления',
                'address' => 'Москва',
            ),

            7 =>
            array (
                'name' => 'Институт экономики и права',
                'address' => 'Москва',
            ),

            8 =>
            array (
                'name' => 'Институт тонких химических технологий им. М.В. Ломоносова',
                'address' => 'Москва',
            ),
        ));
    }
}
