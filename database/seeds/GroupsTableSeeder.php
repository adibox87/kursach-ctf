<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->delete();

        for ($i = 1; $i < 5; $i++) {
            DB::table('groups')->insert([
                    [
                        'shipher' => 'ББСО-0' . $i .'-17',
                        'university_id' => '1'
                    ]
                ]

            );
        }

        for ($i = 1; $i < 5; $i++) {
            DB::table('groups')->insert([
                    [
                        'shipher' => 'ИИСО-0' . $i .'-17',
                        'university_id' => '2'
                    ]
                ]

            );
        }

        for ($i = 1; $i < 5; $i++) {
            DB::table('groups')->insert([
                    [
                        'shipher' => 'КИБО-0' . $i .'-17',
                        'university_id' => '3'
                    ]
                ]

            );
        }

        for ($i = 1; $i < 5; $i++) {
            DB::table('groups')->insert([
                    [
                        'shipher' => 'РИБО-0' . $i .'-17',
                        'university_id' => '4'
                    ]
                ]

            );
        }

        for ($i = 1; $i < 5; $i++) {
            DB::table('groups')->insert([
                    [
                        'shipher' => 'ФРБО-0' . $i .'-17',
                        'university_id' => '5'
                    ]
                ]

            );
        }

        for ($i = 1; $i < 5; $i++) {
            DB::table('groups')->insert([
                    [
                        'shipher' => 'УУБО-0' . $i .'-17',
                        'university_id' => '6'
                    ]
                ]

            );
        }

        for ($i = 1; $i < 5; $i++) {
            DB::table('groups')->insert([
                    [
                        'shipher' => 'ИЭП-0' . $i .'-17',
                        'university_id' => '7'
                    ]
                ]

            );
        }

        for ($i = 1; $i < 5; $i++) {
            DB::table('groups')->insert([
                    [
                        'shipher' => 'ТХБО-0' . $i .'-17',
                        'university_id' => '8'
                    ]
                ]

            );
        }
    }
}
