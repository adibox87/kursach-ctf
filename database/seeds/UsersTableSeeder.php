<?php

use App\Enums\UserRole;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
                [
                    'name' => 'Admin',
                    'email' => 'admin@example.com',
                    'role_id' => '1',
                    'email_verified_at' => NULL,
                    'password' => '$2y$10$KwH6C7yAcpgq2VYXDuhjj.VIn0.lV/ph6OxyMJx6Vla.S1sGnxXoK', // secret
                    'remember_token' => NULL,
                    'group_id' => null,
                    'created_at' => '2018-10-01 18:39:19',
                    'updated_at' => '2018-10-01 18:39:19',
                ],
                [
                    'name' => 'Contestant',
                    'email' => 'contestant@example.com',
                    'role_id' => '2',
                    'email_verified_at' => NULL,
                    'password' => '$2y$10$KwH6C7yAcpgq2VYXDuhjj.VIn0.lV/ph6OxyMJx6Vla.S1sGnxXoK', // secret
                    'remember_token' => NULL,
                    'group_id' => '1',
                    'created_at' => date("Y-m-d H:i:s") ,
                    'updated_at' => date("Y-m-d H:i:s") ,
                ],
                [
                    'name' => 'Teacher',
                    'email' => 'teacher@example.com',
                    'role_id' => '3',
                    'email_verified_at' => NULL,
                    'password' => '$2y$10$KwH6C7yAcpgq2VYXDuhjj.VIn0.lV/ph6OxyMJx6Vla.S1sGnxXoK', // secret
                    'remember_token' => NULL,
                    'group_id' => NULL,
                    'created_at' => date("Y-m-d H:i:s") ,
                    'updated_at' => date("Y-m-d H:i:s") ,
                ]
        ]

        );

        factory(App\User::class, 200)->create();






    }
}
