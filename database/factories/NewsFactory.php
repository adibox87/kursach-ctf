<?php

use Faker\Generator as Faker;

$factory->define(App\News::class, function (Faker $faker) {
    $faker->addProvider(new \DavidBadura\FakerMarkdownGenerator\FakerProvider($faker));

    return [
        'title' => $faker->realText(50),
        'body' => $faker->realText(200),
        'user_id' => \App\User::inRandomOrder()->first(['id'])->id,
    ];
});
