<?php

use Faker\Generator as Faker;

$factory->define(App\Group::class, function (Faker $faker) {
    return [
        'shipher' =>  $faker->company,
        'university_id' => \App\University::inRandomOrder()->first(['id'])->id,
    ];
});
