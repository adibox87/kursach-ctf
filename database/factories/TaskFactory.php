<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    $faker->addProvider(new \DavidBadura\FakerMarkdownGenerator\FakerProvider($faker));

    return [
        'title' => $faker->company,
        'lead' => $faker->realText(),
        'description' => $faker->markdown(),
        'cost' => $faker->randomElement(range(50, 1500, 50)),
        'correct_answer' => 'flag{' . snake_case($faker->words(3, true)) . '}',
        'status' => mt_rand(0, 100) <= 30 ? \App\Enums\TaskStatus::Blocked : \App\Enums\TaskStatus::Open,
        'category_id' => \App\Category::inRandomOrder()->first(['id'])->id,
        'course_id' => \App\Course::inRandomOrder()->first(['id'])->id,
    ];
});
