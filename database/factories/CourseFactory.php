<?php

use Faker\Generator as Faker;

$factory->define(App\Course::class, function (Faker $faker) {
    $faker->addProvider(new \DavidBadura\FakerMarkdownGenerator\FakerProvider($faker));

    return [
        'title' => $faker->company,
        'lead' => $faker->realText(),
        'text' => $faker->markdown(),
        'user_id' => \App\User::inRandomOrder()->first(['id'])->id,
    ];
});
